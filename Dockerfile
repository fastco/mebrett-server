FROM drupal:7
MAINTAINER brett.richardson.nz@gmail.com

# Update APT
RUN apt-get update

# Install Drush
RUN apt-get install php-pear -y && \
    pear channel-discover pear.drush.org && \
    pear install drush/drush

# Install additional server components
RUN apt-get install php5-gd -y

# Install utility software
RUN apt-get install git curl vim zsh -y
