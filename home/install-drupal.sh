# /bin/bash

cd /var/www/html

# Install Drupal
drush site-install minimal \
  --db-url="mysql://root:ishallpass123@db:3306/mebrettdrupal" \
  --account-name="Brett Richardson" \
  --account-pass="ishallpass123" \
  -y

# Download modules
drush dl \
  views token module_filter ctools link backup_migrate devel \
  libraries pathauto memcache wysiwyg link file_entity media \
  entity pathauto token prettify jquery_update \
  rubik bootstrap tao -y

# Create private files directory
mkdir -p sites/default/private
drush vset file_private_path sites/default/private -y

# Set default themes
drush vset theme_default bootstrap -y
drush vset admin_theme rubik -y

# Update Drupal & modules
drush pm-update

# Enable devel modules
drush en devel devel_generate -y
