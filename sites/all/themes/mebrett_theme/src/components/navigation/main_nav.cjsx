React = require 'react'

module.exports = React.createClass
  render: ->
    <div className="els-main-nav navbar">
      <div className="container">
        <div className="pull-right">
          <a className="btn btn-link" href="#">home</a>
          <a className="btn btn-link" href="#">github</a>
          <a className="btn btn-link" href="#">portfolio</a>
          <a className="btn btn-link" href="#">skills</a>
          <a className="btn btn-link" href="#">about</a>
          <a className="btn btn-link" href="#">contact</a>
        </div>
      </div>
    </div>
