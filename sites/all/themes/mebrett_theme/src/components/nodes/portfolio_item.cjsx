React   = require 'react'
TagList = React.createClass render: -> <p>TagList</p>

module.exports = React.createClass
  render: ->
    <article>
      <img src="http://placehold.it/800x250" />

      <h2>PortfolioItem</h2>

      <div className="row">
        <div className="col-sm-7">
          <p>
            Vestibulum sit amet luctus ex.
            Nullam in magna quis purus mollis commodo ac nec risus.
            Sed vitae commodo mi. Sed quis posuere mi. Nam nisl ex, egestas
            ut libero vel, rhoncus commodo velit. Nullam non metus ligula.
          </p>

          <p>
            Proin erat quam, vulputate tempus est vel, faucibus imperdiet leo.
            In cursus mauris ut magna ultricies, a vestibulum turpis elementum.
            Vestibulum vitae consectetur ex. Vivamus volutpat leo quis arcu
            tristique rutrum ac vel nisi.
          </p>
        </div>

        <TagList className="col-sm-5" />
      </div>
    </article>



