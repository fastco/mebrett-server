React         = require 'react'
Container     = require './utils/container'
PortfolioItem = require './nodes/portfolio_item'

module.exports = React.createClass
  render: ->
    <section id="content">
      <Container>
        <h2>What I've been up to recently...</h2>

        <PortfolioItem />
        <PortfolioItem />
        <PortfolioItem />
      </Container>
    </section>
