React     = require 'react'
MainNav   = require './navigation/main_nav'
Container = require './utils/container'
PromoArea = require './promo_area'
SearchBar = React.createClass render: -> <p>SearchBar</p>

module.exports = React.createClass
  render: ->
    <header id="Header">
      <Container>
        <MainNav page=@props.page></MainNav>
        <PromoArea></PromoArea>
        <SearchBar></SearchBar>
      </Container>
    </header>



