React        = require 'react'
Container    = require './utils/container'
SubNav       = require './navigation/sub_nav'
BookPromo    = React.createClass render: -> <p>BookPromo</p>
ContactLinks = React.createClass render: -> <p>ContactLinks</p>

module.exports = React.createClass
  render: ->
    <footer>
      <Container>
        <BookPromo />

        <div className="row">
          <div className="col-sm-6">
            <SubNav />
          </div>

          <div className="col-sm-6">
            <ContactLinks />
          </div>
        </div>
      </Container>
    </footer>
