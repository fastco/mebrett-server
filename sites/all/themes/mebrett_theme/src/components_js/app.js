var Content, Footer, Header, Page, React;

React = require('react');

Page = require('./page');

Header = require('./header');

Content = require('./content');

Footer = require('./footer');

module.exports = React.createClass({
  render: function() {
    return React.createElement("div", null, React.createElement(Header, null), React.createElement(Content, null), React.createElement(Footer, null));
  }
});
