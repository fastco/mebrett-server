var Container, MainNav, PromoArea, React, SearchBar;

React = require('react');

MainNav = require('./navigation/main_nav');

Container = require('./utils/container');

PromoArea = require('./promo_area');

SearchBar = React.createClass({
  render: function() {
    return React.createElement("p", null, "SearchBar");
  }
});

module.exports = React.createClass({
  render: function() {
    return React.createElement("header", {
      "id": "Header"
    }, React.createElement(Container, null, React.createElement(MainNav, null), React.createElement(PromoArea, null), React.createElement(SearchBar, null)));
  }
});
