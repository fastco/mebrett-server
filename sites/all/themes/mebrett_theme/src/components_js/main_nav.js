var React;

React = require('react');

module.exports = React.createClass({
  render: function() {
    return React.createElement("div", {
      "className": "els-main-nav navbar"
    }, React.createElement("div", {
      "className": "container"
    }, React.createElement("div", {
      "className": "pull-right"
    }, React.createElement("a", {
      "className": "btn btn-link",
      "href": "#"
    }, "home"), React.createElement("a", {
      "className": "btn btn-link",
      "href": "#"
    }, "github"), React.createElement("a", {
      "className": "btn btn-link",
      "href": "#"
    }, "portfolio"), React.createElement("a", {
      "className": "btn btn-link",
      "href": "#"
    }, "skills"), React.createElement("a", {
      "className": "btn btn-link",
      "href": "#"
    }, "about"), React.createElement("a", {
      "className": "btn btn-link",
      "href": "#"
    }, "contact"))));
  }
});
