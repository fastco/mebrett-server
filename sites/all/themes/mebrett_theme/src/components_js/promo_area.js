var HeadingTicker, IconLink, React;

React = require('react');

HeadingTicker = React.createClass({
  render: function() {
    return React.createElement("p", null, "HeadingTicker");
  }
});

IconLink = React.createClass({
  render: function() {
    return React.createElement("p", null, "IconLink");
  }
});

module.exports = React.createClass({
  render: function() {
    return React.createElement("div", {
      "className": "row"
    }, React.createElement("div", {
      "className": "col-sm-7"
    }, React.createElement("img", {
      "src": "http://placehold.it/400x300"
    })), React.createElement("div", {
      "className": "col-sm-5"
    }, React.createElement("h1", null, "I\'m Brett"), React.createElement(HeadingTicker, null), React.createElement(IconLink, {
      "href": "http://github.com/brett-richardson",
      "icon": "github"
    }, "Github"), React.createElement(IconLink, {
      "href": "/blog",
      "icon": "blog"
    }, "Blog")));
  }
});
