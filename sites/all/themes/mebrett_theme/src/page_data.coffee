module.exports = {
  currentState: {
    page: 'home'
  },

  portfolioItems: [ {
    title: "PortfolioItem Title 1",
    content: "Blah blah"
  },{
    title: "PortfolioItem Title 2",
    content: "Blah blah"
  } ],

  articles: [ {
    title: "Article Title 1",
    content: "Blah blah"
  }, {
    title: "Article Title 2",
    content: "Blah blah"
  } ]
}
