gulp       = require 'gulp'
bower      = require 'gulp-bower'
sass       = require 'gulp-sass'
cjsx       = require 'gulp-cjsx'
browserify = require 'gulp-browserify'
rename     = require 'gulp-rename'

gulp.task 'default', ['build', 'watch']

# Main

gulp.task 'build', [
  'build_bower', 'build_styles', 'build_browserify' # 'build_components',
]

gulp.task 'watch', ['watch_styles', 'watch_coffee']

# Builds

gulp.task 'build_bower', ->
  bower(directory: './bower_components')

gulp.task 'build_styles', ->
  gulp.src './src/main.scss'
    .pipe sass()
    .pipe gulp.dest './css'

gulp.task 'build_components', ->
  gulp.src './src/components/**/*.cjsx'
    .pipe cjsx(bare: true).on('error', console.log)
    .pipe gulp.dest './src/components_js'

gulp.task 'build_browserify', ->
  gulp.src('./src/main.coffee', read: false)
    .pipe browserify(
      transform: ['coffee-reactify'],
      extensions: ['.coffee', '.cjsx']
    )
    .pipe rename 'main.js'
    .pipe gulp.dest './js'

# Watches

gulp.task 'watch_styles', ->
  gulp.watch ['./src/main.scss', './src/scss/**/*.scss'], ['build_styles']

gulp.task 'watch_coffee', ->
  gulp.watch [
    './src/main.coffee', './src/components/**/*.cjsx'
  ], ['build_browserify']
